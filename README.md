
Disk Mount Role: redhat-disk-mount ( Optional) 

Mount new disk in remote machine enable this role and modified the "disk name" and "Ansible mounts" 
variable value in config.yml

1. This role will created partition in remote machine
2. Change the file system 
3. Format the disk
4. Mount the disk
5. Make entry in fstab about the mount to mount the disk from boot
